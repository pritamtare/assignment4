
import React from "react";
import FetchApi from './fetchapi/FetchApi'
import '../node_modules/bootstrap/dist/css/bootstrap.css'

export default function App() {
  return (
    <div className="App">
      <FetchApi />
    </div>
  );
}
